import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.junit.Test;

import junit.framework.Assert;
import pong_game.Ball;
import pong_game.GameStateCapture;
import pong_game.NetCommands;
import pong_game.Player;
import pong_server.Game;
import pong_server.PongServer;

public class ServerTest {
	
	public void setUp() {
		
	}

	@Test
	public void testWelcome() {
		// fail("Not yet implemented");
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					new PongServer().main(new String[]{""});
				} catch (IOException e) { e.printStackTrace(); }
			}
		}).start();
		
		DummyClient dummy1 = new DummyClient();
		DummyClient dummy2 = new DummyClient();
		
		BufferedReader socketIn = dummy1.getSocketIn();
		PrintWriter socketOut = dummy1.getSocketOut();
		
		String received;
		Integer command = new Integer(-1);
		String arguments = "";
		
		GameStateCapture gsc;
		GameStateCapture newGsc;
		
		System.out.println("Using Player 1 as main player.");
		
		try {
			// Test WELCOME message of server
			received = socketIn.readLine();
			System.out.println("Received: "  +received);
			command = retrieveCommand(received);
			arguments = retrieveArguments(received);
			assertTrue(command == NetCommands.Server.WELCOME);
			assertTrue(Integer.parseInt(arguments) == 1);
			
			
			
			// Test MESSAGE message of server
			received = socketIn.readLine();
			System.out.println("Received: "  +received);
			command = retrieveCommand(received);
			arguments = retrieveArguments(received);
			assertTrue(command == NetCommands.Server.MESSAGE);
			assertTrue(arguments.length() > 0);
			
			
			
			// Test 2nd MESSAGE message of server
			received = socketIn.readLine();
			System.out.println("Received: "  +received);
			command = retrieveCommand(received);
			arguments = retrieveArguments(received);
			assertTrue(command == NetCommands.Server.MESSAGE);
			assertTrue(arguments.length() > 0);
			
			
			
			// Send IDLE to server to retrieve game state
			socketOut.println(NetCommands.Client.IDLE);

			// check game state
			received = socketIn.readLine();
			System.out.println("Received: "  +received);
			command = retrieveCommand(received);
			arguments = retrieveArguments(received);
			assertTrue(command == NetCommands.Server.SEND_STATE);
			assertTrue(arguments.length() > 0);
			
			gsc = new GameStateCapture(arguments);
			
			// check that the player Y positions is within the game's FIELD_HEIGHT
			assertTrue(gsc.getPlayer1Coords()[1] < Game.FIELD_HEIGHT);
			assertTrue(gsc.getPlayer1Coords()[1] > 0);
			assertTrue(gsc.getPlayer2Coords()[1] < Game.FIELD_HEIGHT);
			assertTrue(gsc.getPlayer2Coords()[1] > 0);
			
			
			
			// Move player upwards and retrieve game state
			socketOut.println(NetCommands.Client.MOVE_UP);
			socketOut.println(NetCommands.Client.IDLE);
			
			received = socketIn.readLine();
			System.out.println("Received: "  +received);
			command = retrieveCommand(received);
			arguments = retrieveArguments(received);
			newGsc = new GameStateCapture(arguments);
			
			// check if the player really has moved up
			assertTrue(newGsc.getPlayer1Coords()[1] < gsc.getPlayer1Coords()[1]);
			
			
			
			// Move player downwards and retrieve game state
			socketOut.println(NetCommands.Client.MOVE_DOWN);
			socketOut.println(NetCommands.Client.IDLE);
			gsc = newGsc;
			received = socketIn.readLine();
			System.out.println("Received: "  +received);
			command = retrieveCommand(received);
			arguments = retrieveArguments(received);
			newGsc = new GameStateCapture(arguments);
			
			// check if the player really has moved down
			assertTrue(newGsc.getPlayer1Coords()[1] > gsc.getPlayer1Coords()[1]);
			
			
			
			// Move the ball and retrieve game state
			socketOut.println(NetCommands.Client.FIRE);
			Thread.sleep(2000);	// there's a known issue that the ball won't move for a few seconds
			socketOut.println(NetCommands.Client.IDLE);
			gsc = newGsc;
			received = socketIn.readLine();
			System.out.println("Received: "  +received);
			command = retrieveCommand(received);
			arguments = retrieveArguments(received);
			newGsc = new GameStateCapture(arguments);
			
			// check if the player really has moved down
			assertTrue(newGsc.getBallCoords()[1] != gsc.getBallCoords()[1]);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
	private String retrieveArguments(String received) {
		String arguments = "";
		int separatorIndex = received.indexOf(' ');
		
		if (separatorIndex > 0) {
			arguments = received.substring(separatorIndex).trim();
		}
		return arguments;
	}
	
	private int retrieveCommand(String received) {
		int command = -1;
		int separatorIndex = received.indexOf(' ');
		
		if (separatorIndex > 0) {
			command = Integer.parseInt(received.substring(0, separatorIndex));
		} else {
			command = Integer.parseInt(received);
		}
		
		return command;
	}

	private class DummyClient /* implements Runnable */{
		private static final int PORT = 12342;
		private static final String ADDRESS = "localhost";

		private Socket socket;
		private BufferedReader socketIn;
		private PrintWriter socketOut;
		
		private Player player = null;	// alias for client's player
		private Player opponent = null;
		
		private Ball ball;
		private int playerID = -1;
		
		public DummyClient() {
			try {
				socket = new Socket(ADDRESS, PORT);
				socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				socketOut = new PrintWriter(socket.getOutputStream(), true);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		public Socket getSocket() {
			return socket;
		}
		
		public BufferedReader getSocketIn() {
			return socketIn;
		}
		
		public PrintWriter getSocketOut() {
			return socketOut;
		}
	}
}
