package pong_client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JPanel;

import pong_game.Ball;
import pong_game.GameStateCapture;
import pong_game.NetCommands;
import pong_game.Player;
import pong_server.Game;

public class PongClientPanel extends JPanel implements Runnable {
	private Player playerLeft;
	private Player playerRight;
	private Player player = null;	// alias for client's player
	private Player opponent = null;

	private Ball ball;
	private int playerID = -1;

	private Socket socket;
	private BufferedReader socketIn;
	private PrintWriter socketOut;

	private final Color PLAYER_COLOR = Color.GREEN;
	private final Color OPPONENT_COLOR = Color.RED;
	private final Color BALL_COLOR = Color.ORANGE;

	private final int SCREEN_WIDTH = Game.FIELD_WIDTH;
	private final int SCREEN_HEIGHT = Game.FIELD_HEIGHT;
	private final int UPDATE_PERIOD = 1000 / 60; // Draw thread's repetition cycle

	private final Font GAME_FONT;
	private final RenderingHints GAME_FONT_RH;
	private final int FONT_SIZE = 60;
	private final Color LIFE_FONT_COLOR = Color.DARK_GRAY;

	public PongClientPanel(String serverAddress, int serverPort) {
		GAME_FONT = new Font("Consolas", Font.PLAIN, FONT_SIZE);
		GAME_FONT_RH = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		GAME_FONT_RH.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

		addEventListeners();
		initialisePanel();
		initialiseActors();
		connectToServer(serverAddress, serverPort);
	}

	private void initialisePanel() {
		setFocusable(true);
		setBackground(Color.BLACK);
		setPreferredSize(new Dimension(Game.FIELD_WIDTH, Game.FIELD_HEIGHT));
		setDoubleBuffered(true);
	}

	private void addEventListeners() {
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {

				switch (e.getKeyCode()) {
				case KeyEvent.VK_UP:
					socketOut.println("" + NetCommands.Client.MOVE_UP);
					break;

				case KeyEvent.VK_DOWN:
					socketOut.println("" + NetCommands.Client.MOVE_DOWN);
					break;

				case KeyEvent.VK_SPACE:
					socketOut.println("" + NetCommands.Client.FIRE);
					break;
				}
			}
		});
	}

	private void initialiseActors() {
		playerLeft = new Player();
		playerRight = new Player();
		ball = new Ball();
		
		// placeholder references until playerID gets set.
		player = playerLeft;
		opponent = playerRight;
	}

	private void connectToServer(String serverAddr, int serverPort) {
		try {
			socket = new Socket(serverAddr, serverPort);
			socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			socketOut = new PrintWriter(socket.getOutputStream(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		drawLifeCounters(g, playerLeft, playerRight);
		drawPlayer(g, playerLeft);
		drawPlayer(g, playerRight);
		drawBall(g, ball);
		drawClientStatus(g);
	}

	private void drawLifeCounters(Graphics g, Player pLeft, Player pRight) {
		Graphics2D g2d = (Graphics2D) g;

		g2d.setColor(LIFE_FONT_COLOR);
		g2d.setFont(GAME_FONT);
		g2d.setRenderingHints(GAME_FONT_RH);

		int leftX = SCREEN_WIDTH / 4;
		int rightX = SCREEN_WIDTH * 3 / 4;

		if (playerID == 1) {
			g2d.drawString("" + player.getLives(), leftX, FONT_SIZE + 30);
			g2d.drawString("" + opponent.getLives(), rightX, FONT_SIZE + 30);
		} else {
			g2d.drawString("" + player.getLives(), rightX, FONT_SIZE + 30);
			g2d.drawString("" + opponent.getLives(), leftX, FONT_SIZE + 30);
		}
	}

	private synchronized void drawPlayer(Graphics g, Player p) {
		Graphics2D g2d = (Graphics2D) g;

		if (p == player) {
			g2d.setColor(PLAYER_COLOR);
		}
		else {
			g2d.setColor(OPPONENT_COLOR);
		}

		g2d.fillRect(p.getX(), p.getY(), p.getWidth(), p.getHeight());

		Toolkit.getDefaultToolkit().sync();
	}

	private void drawBall(Graphics g, Ball b) {
		Graphics2D g2d = (Graphics2D) g;

		g2d.setColor(BALL_COLOR);
		g2d.fillRect((int) b.getX(), (int) b.getY(), b.getWidth(), b.getHeight());

		Toolkit.getDefaultToolkit().sync();
	}

	private void drawClientStatus(Graphics g) {
		Player deceased = getDeceasedPlayer(); 
		if (deceased != null) {
			Graphics2D g2d = (Graphics2D) g;
			String message = "";

			if (deceased.equals(player)) {
				message = "You LOSE";
				g2d.setColor(Color.RED);
			}
			else {
				message = "You WIN";
				g2d.setColor(Color.GREEN);
			}

			g2d.setFont(GAME_FONT);
			g2d.setRenderingHints(GAME_FONT_RH);
			g2d.drawString(message, (int)(SCREEN_WIDTH / 3.25), SCREEN_HEIGHT / 2);
			Toolkit.getDefaultToolkit().sync();
		}
	}
	
	private Player getDeceasedPlayer() {
		if (player != null && opponent != null) {
			if (player.getLives() <= 0)
				return player;
			else if (opponent.getLives() <= 0)
				return opponent;	
		}
		return null;
	}

	@Override
	public void addNotify() {
		super.addNotify();

		new Thread(new ClientConnectionThread()).start();
		new Thread(this).start();
	}

	// UI drawing/repainting thread
	@Override
	public void run() {
		try {
			long timerStart, sleep;
			while (true) {
				timerStart = System.currentTimeMillis();
				repaint();

				// Maintain 60 FPS
				sleep = UPDATE_PERIOD - System.currentTimeMillis() + timerStart;
				if (sleep < 0)
					sleep = 0;
				Thread.sleep(sleep);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class ClientConnectionThread implements Runnable {

		@Override
		public void run() {
			try {
				// while (true) {
				System.out.println("Retrieving from server");

				while (true) {
					// String received;
					String received = socketIn.readLine();
					int separatorIndex = received.indexOf(' ');
					String arguments = "";
					int command = -1;

					// get command and arguments from received string
					if (separatorIndex > 0) {
						arguments = received.substring(separatorIndex).trim();
						command = Integer.parseInt(received.substring(0, separatorIndex));
					} else {
						command = Integer.parseInt(received);
					}

					switch (command) {

					case NetCommands.Server.WELCOME:
						System.out.println("Client connected successfull.");
						// set ID so we know which player we are controlling
						playerID = Integer.parseInt(arguments);

						// distinguish players
						if (playerID == 1) {
							player = playerLeft;
							opponent = playerRight;
						} else if (playerID == 2) {
							player = playerRight;
							opponent = playerLeft;
						}

						break;

					case NetCommands.Server.MESSAGE:
						System.out.println(arguments);
						break;

					case NetCommands.Server.SEND_STATE:
						setLocalGameState(new GameStateCapture(arguments));
						break;

					case NetCommands.Server.WIN:
					case NetCommands.Server.LOSE:
						// socketOut.println("" + NetCommands.Client.QUIT);
						// socket.close();
						break;

					default:
					}
					// tell server this client is still connected
					socketOut.println("" + NetCommands.Client.IDLE);
				}
				// }
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}

	private synchronized void setLocalGameState(GameStateCapture gsc) {
		int[] ballPos = gsc.getBallCoords();
		ball.setX(ballPos[0]);
		ball.setY(ballPos[1]);
	
		player.setLives(gsc.getPlayer1Lives());
		player.setX(gsc.getPlayer1Coords()[0]);
		player.setY(gsc.getPlayer1Coords()[1]);
		
		opponent.setLives(gsc.getPlayer2Lives());
		opponent.setX(gsc.getPlayer2Coords()[0]);
		opponent.setY(gsc.getPlayer2Coords()[1]);
	}
}
