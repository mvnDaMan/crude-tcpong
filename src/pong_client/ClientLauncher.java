package pong_client;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class ClientLauncher extends JFrame {
	String serverAddress;
	int serverPort;
	
	public ClientLauncher(String sAddr, int sPort) { 
		super();
		serverAddress = sAddr;
		serverPort = sPort;
		
		initUI();
	}
	
	private void initUI() {
		add(new PongClientPanel(serverAddress, serverPort));
		setResizable(false);
		pack();
		setTitle("TCPong Client");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Usage: java pong_client.ClientLauncher <IP> <PORT>");
			return;
		}
		
		String serverAddress = args[0];
		int serverPort = Integer.parseInt(args[1]);
		
		System.out.println(serverAddress);
		System.out.println(serverPort);
		
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				ClientLauncher cl = new ClientLauncher(serverAddress, serverPort);
				cl.setVisible(true);
			}
		});
	}
}
