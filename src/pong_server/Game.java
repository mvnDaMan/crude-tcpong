// package pong_game;
package pong_server;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.net.ServerSocket;

import pong_game.Ball;
import pong_game.Player;

public class Game {

	private final int UPDATE_PERIOD = 1000 / 60;	// update 60x per second
	private Player[] players;
	private Player currentBaller; // player who has the ball
	private Player player1;			// alias for players[0]
	private Player player2;			// alias for players[1]
	private int ballerIndex = 0; // player who has the ball (index)
	private Ball ball;

	public static final int FIELD_WIDTH = 800;
	public static final int FIELD_HEIGHT = 400;
	
	private final int PLAYER_LIVES_EACH = 3;
	public static final int INITIAL_BALL_SPEED = 5;
	private final double BALL_SPEED_INCREMENTS = 0.5; 
	
	private final int PLAYER_Y_MIDDLE = (Game.FIELD_HEIGHT - Player.HEIGHT) / 2;
	private final int PLAYER_X_MARGIN = 20;
	
	private ServerSocket listener;

	
	// private boolean isPlaying = true;

	public Game(ServerSocket ss) throws Exception {
		listener = ss;
		
		player1 = new Player();
		player2 = new Player();
		ball = new Ball(Math.PI / 4, INITIAL_BALL_SPEED);

		System.out.println("Waiting for players...");
		PlayerThread p1Thread = new PlayerThread(1, listener.accept(), player1, player2, ball);
		PlayerThread p2Thread = new PlayerThread(2, listener.accept(), player2, player1, ball);

		System.out.println("Both players have joined!\nGame starting!");
		
		players = new Player[]{ player1, player2 };
		currentBaller = player1;

		player1.setLives(PLAYER_LIVES_EACH);
		player2.setLives(PLAYER_LIVES_EACH);
		
		resetPlayerPositions();
		
		(new Thread(p1Thread)).start();
		(new Thread(p2Thread)).start();
	}
	
	private void resetPlayerPositions() {
		player1.setX(PLAYER_X_MARGIN);
		player1.setY(PLAYER_Y_MIDDLE);
		player1.hasFiredBall(false);
		
		player2.setX(Game.FIELD_WIDTH - Player.WIDTH - PLAYER_X_MARGIN);
		player2.setY(PLAYER_Y_MIDDLE);
		player2.hasFiredBall(false);
	}

	public void start() {
		long sleep, timerStart;
		while (players[0].getLives() > 0 && players[1].getLives() > 0) {
			try {
				timerStart = System.currentTimeMillis();
				
				update();
				
				sleep = UPDATE_PERIOD - (System.currentTimeMillis() - timerStart);
				if (sleep < 0)
					sleep = 0;
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void update() {
		// update players
		for (int i = 0; i < players.length; i++) {
			// correct the player's Y position
			int pY = players[i].getY();
			if (pY < 0) {
				players[i].setY(0);
			}
			if (pY + Player.HEIGHT > FIELD_HEIGHT) {
				players[i].setY(FIELD_HEIGHT - Player.HEIGHT);
			}
		}

		updateBall();
	}

	private void updateBall() {
		if (currentBaller.hasFiredBall()) {
			// check collisions with top/bottom walls
			if (ball.getY() < 0) {
				ball.setY(0);
				ball.deflectHorizontalAxis();
			} else {
				int ballBottomWallY = FIELD_HEIGHT - ball.HEIGHT;
				if (ball.getY() > ballBottomWallY) {
					ball.setY(ballBottomWallY);
					ball.deflectHorizontalAxis();
				}
			}
			
			handleBallCollision();

			if (!isBallInField()) {
				int bx = (int) ball.getX();

				// if player 1 (left) didn't catch the ball.
				if (bx < players[0].getX()) {
					players[0].decrementLives();
				}

				// if player 1 (left) didn't catch the ball.
				if (bx > players[1].getX()) {
					players[1].decrementLives();
				}

				resetPlayerPositions();
				swapBaller();
				ball.resetSpeed();
			}

			// only move the ball once it has been fired
			ball.move();
		} else {
			// ball follow player if ball has not been fired yet
			ballFollowBaller();
		}
	}
	
	/*
	 * private boolean collide(Rectangle a, Rectangle b) { a.
	 * 
	 * return false; }
	 */

	private void handleBallCollision() {
		Rectangle ballRect = new Rectangle(
				new Point((int)ball.getX(), (int)ball.getY()),
				new Dimension(ball.getWidth(), ball.getHeight()));
		Rectangle playerRect;
		Player p;
		boolean collision = false;
		
		// ball is going right, then player 2 must catch it
		if (ball.getXVelocity() > 0) {
			p = players[1];
			playerRect = new Rectangle(
					new Point(p.getX(), p.getY()),
					new Dimension(p.getWidth(), p.getHeight()));
			
			if (ballRect.intersects(playerRect)) {
				ball.setX((int)p.getX() - ball.getWidth());
				collision = true;
			}
		}
		else {
			p = players[0];
			playerRect = new Rectangle(
					new Point(p.getX(), p.getY()),
					new Dimension(p.getWidth(), p.getHeight()));
			
			if (ballRect.intersects(playerRect)) {
				ball.setX((int)p.getX() + p.getWidth());
				collision = true;
			}
		}
		
		if (collision) {
			ball.deflectVerticalAxis();
			double bs = ball.getSpeed();
			// speed it up
			ball.setSpeed(bs > 0 ? bs + BALL_SPEED_INCREMENTS : bs - BALL_SPEED_INCREMENTS);
		}
	}
	
	private void ballFollowBaller() {
		if (currentBaller.equals(players[0])) {
			ball.setX(currentBaller.getX() + Player.WIDTH);
		}
		else {
			ball.setX(currentBaller.getX() - Ball.WIDTH);
		}
		ball.setY(currentBaller.getY() + (Player.HEIGHT - Ball.HEIGHT) / 2);
	}

	// checks if ball's x position is within screen's width
	private boolean isBallInField() {
		float x = ball.getX();
		int w = ball.getWidth();

		if (x > -w && x < FIELD_WIDTH) {
			return true;
		}
		return false;
	}

	// Changes the current baller between player 1 or 2
	private void swapBaller() {
		ballerIndex = (ballerIndex + 1) % players.length;
		currentBaller = players[ballerIndex];
		ball.setSpeed(-ball.getSpeed());
	}
}
