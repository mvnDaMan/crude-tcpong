// Server code and design inspired by:
// http://cs.lmu.edu/~ray/notes/javanetexamples/

package pong_server;

import java.io.IOException;
import java.net.ServerSocket;

import pong_game.Ball;
import pong_game.Player;

public class PongServer {
	// public static final String SERVER_ADDRESS = "localhost";
	public static final int SERVER_PORT = 12342;

	public static void main(String[] args) throws IOException {
		ServerSocket listener = null;
		try {
			listener = new ServerSocket(SERVER_PORT);
			System.out.println("Pong server online");

			while (true) {
				new Game(listener).start();
				System.out.println("Game has ended.\n!");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			listener.close();
		}
	}

}
