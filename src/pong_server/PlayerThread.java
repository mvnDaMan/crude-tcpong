// TCP protocol inspired by examples from:
// http://cs.lmu.edu/~ray/notes/javanetexamples/

package pong_server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import pong_game.Ball;
import pong_game.GameStateCapture;
import pong_game.NetCommands;
import pong_game.Player;


public class PlayerThread implements Runnable {
	
	private int threadID;
	private Socket socket;
	private Player player;
	private Player opponent;
	private Ball ball;
	private PrintWriter socketOut;
	private BufferedReader socketIn;
	
	public PlayerThread(int pID, Socket s, Player p, Player opp, Ball b) {
		threadID = pID;
		socket = s;
		player = p;
		opponent = opp;
		ball = b;
		
		try {
			socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			socketOut = new PrintWriter(socket.getOutputStream(), true);
			send(NetCommands.Server.WELCOME, Integer.toString(pID));
			send(NetCommands.Server.MESSAGE, "Waiting for opponent to connect");
			
		} catch (IOException e) {
			// Player died???
			e.printStackTrace();
		}
	}

	private void send(int command, String params) {
		socketOut.println(command + " " + params);
	}
	
	private void send(int command) {
		socketOut.println(command);
	}
	
	@Override
	public void run() {
		send(NetCommands.Server.MESSAGE, "All players have connected");
		
		// Repeatedly get commands from the client and process them
		try {
			
			while (true) {
				String received = socketIn.readLine();
				// System.out.println("received: " + received);
				int command;
				int commandSeparator = received.indexOf(' ');

				// get command
				if (commandSeparator > 0) {
					command = Integer.parseInt(received.substring(0, commandSeparator));
				} else {
					command = Integer.parseInt(received);
				}

				switch (command) {
				case NetCommands.Client.FIRE:
					player.fireBall();
					break;
					
				case NetCommands.Client.MOVE_UP:
					player.moveUp();
					break;
					
				case NetCommands.Client.MOVE_DOWN:
					player.moveDown();
					break;
					
				case NetCommands.Client.IDLE:
					GameStateCapture gsc = new GameStateCapture(player, opponent, ball);
					send(NetCommands.Server.SEND_STATE, gsc.toString());
					break;
					
				case NetCommands.Client.QUIT:
				default:
					// kill both players so Game (main thread) object will end.
					player.setLives(-1);
					opponent.setLives(-1);
					return;
				}
			}
		} catch (IOException e) {
			System.out.println("Player " + threadID + " has disconnected.");
			// e.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
