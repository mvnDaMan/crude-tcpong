package pong_game;

import java.io.Serializable;

// Class that stores the current game's state
public class GameStateCapture implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int player_x;
	private final int player_y;
	private final int opponent_x;
	private final int opponent_y;
	private final int ball_x;
	private final int ball_y;
	
	private final int player_life;
	private final int opponent_life;
	
	public GameStateCapture(Player p, Player o, Ball b) {
		player_x = p.getX();
		player_y = p.getY();
		opponent_x = o.getX();
		opponent_y = o.getY();
		ball_x = (int)b.getX();
		ball_y = (int)b.getY();
		player_life = p.getLives();
		opponent_life = o.getLives();
	}
	
	public GameStateCapture(String description) throws Exception {
		description = description.trim();
		String[] arguments = description.split(" ");
		
		if (arguments.length == 8) {
			player_x = Integer.parseInt(arguments[0]);
			player_y = Integer.parseInt(arguments[1]);
			opponent_x = Integer.parseInt(arguments[2]);
			opponent_y = Integer.parseInt(arguments[3]);
			ball_x = Integer.parseInt(arguments[4]);
			ball_y = Integer.parseInt(arguments[5]);
			player_life = Integer.parseInt(arguments[6]);
			opponent_life = Integer.parseInt(arguments[7]);
		}
		else {
			throw new Exception(this.getClass().getSimpleName() + ": not enough info to create object!");
		}
	}
	
	public int[] getPlayer1Coords() {
		return new int[] {player_x, player_y};
	}
	
	public int[] getPlayer2Coords() {
		return new int[] {opponent_x, opponent_y};
	}
	
	public int[] getBallCoords() {
		return new int[] {ball_x, ball_y};
	}
	
	public int getPlayer1Lives() { return player_life; };
	public int getPlayer2Lives() { return opponent_life; };
	
	@Override
	public String toString() {
		return player_x + " " +
				player_y + " " +
				opponent_x + " " +
				opponent_y + " " + 
				ball_x + " " +
				ball_y + " " +
				player_life + " " +
				opponent_life;
	}
}
