package pong_game;

import pong_server.Game;

public class Player {
	private int x;
	private int y;
	// public static final int HEIGHT = Game.FIELD_HEIGHT - 10;	// lol
	public static final int HEIGHT = Game.FIELD_HEIGHT / 2 - 10;
	public static final int WIDTH = 10;
	public static final int SPEED = 10; 
	
	private int lives = 0;
	
	private boolean ballFired = false;
	
	public Player() {
	}
	
	public Player(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	// public static int getSpeed() { return SPEED; }
	public synchronized int getX() { return x; }
	public synchronized int getY() { return y; }
	public synchronized void setY(int newY) { y = newY; }
	public synchronized void setX(int newX) { x = newX; }
	// public synchronized void setX(int newY) { y = newY; }
	
	public static int getWidth() { return WIDTH; }
	public static int getHeight() { return HEIGHT; }
	
	public synchronized void moveUp() {
		y -= SPEED;
	}
	
	public synchronized void moveDown() {
		y += SPEED;
	}
	
	public synchronized int getLives() { return lives; }
	public synchronized void setLives(int lives) { this.lives = lives; }
	public synchronized void decrementLives() { this.lives--; }
	
	public synchronized boolean hasFiredBall() { return ballFired; }
	public synchronized void hasFiredBall(boolean f) { ballFired = f; }
	public synchronized void fireBall() { ballFired = true; }
}
