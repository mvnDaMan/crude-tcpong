package pong_game;

public class NetCommands {
	public static class Server {
		public static final int MESSAGE = 100;
		public static final int WELCOME = 101;
		public static final int WIN = 102;
		public static final int LOSE = 103;
		public static final int SEND_STATE = 104;
	};

	public static class Client {
		public static final int MOVE_UP = 201;
		public static final int MOVE_DOWN = 202;
		public static final int FIRE = 203;
		public static final int IDLE = 204;
		public static final int QUIT = 205;
		public static final int REMATCH = 206;
	};
}

