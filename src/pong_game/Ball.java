package pong_game;

public class Ball {
	public static final int WIDTH = 10;
	public static final int HEIGHT = WIDTH;
	
	private float x;
	private float y;
	private double xVel;
	private double yVel;
	private double angle;
	private double speed;
	
	public final double ORIGINAL_SPEED;
	
	public Ball() {
		ORIGINAL_SPEED = 0;
	}
	
	public Ball(double angle, double speed) {
		ORIGINAL_SPEED = speed;
		setAngle(angle);
		setSpeed(speed);
	}
	
	public synchronized double getSpeed() { return speed; }
	public synchronized float getX() { return x; }
	public synchronized float getY() { return y; }
	public synchronized void setX(float x) { this.x = x; }
	public synchronized void setY(float y) { this.y = y; }
	public int getWidth() { return WIDTH; }
	public int getHeight() { return HEIGHT; }
	
	public synchronized void move() {
		x += xVel;
		y += yVel;
	}
	
	public synchronized double getXVelocity() { return xVel; }
	public synchronized double getYVelocity() { return yVel; }
	
	public synchronized void deflectVerticalAxis() {
		double incident;
		
		// works well enough
		if (angle > 3 / 4 * 2 * Math.PI) {
			incident = angle - (Math.PI * 2 * 3f / 4);
		}
		else if (angle > Math.PI) {
			incident = angle - Math.PI;
		}
		else if (angle > Math.PI / 2) {
			incident = angle - (Math.PI / 2);
		}
		else {
			incident = angle;
		}
		setAngle(1.5 * Math.PI - incident);
	}
	
	public synchronized void deflectHorizontalAxis() {
		// yVel *= -1;
		setAngle(2 * Math.PI - angle);
	}
	
	public synchronized void setAngle(double a) {
		angle = a;
		setSpeed(speed);
	}
	
	public synchronized void setSpeed(double d) {
		speed = d;
		xVel = speed * Math.cos(angle);
		yVel = speed * Math.sin(angle);
	}
	
	public synchronized void resetSpeed() {
		setSpeed(ORIGINAL_SPEED);
	}
}
